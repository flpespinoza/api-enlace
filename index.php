<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Access-Control-Allow-Methods: POST");
header("Access-Control-Max-Age: 3600");
header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");

/**
 * Metodos venta, ajuste, consultar, eliminar, obtener_saldo
 */

$uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
$uri = explode( '/', $uri );
$method = $uri[1];

// Validar que el metodo sea post
$requestMethod = $_SERVER["REQUEST_METHOD"];
if($requestMethod != 'POST')
{
    header("HTTP/1.1 404 Not Found");
    exit();
}

//Validar que el metodo solicitado exita
if(!in_array($method, ['venta', 'ajuste', 'consultar', 'eliminar', 'obtener-saldo']))
{
    header("HTTP/1.1 404 Not Found");
    exit();
}

//Incluir clase API y llamar al método
require_once __DIR__ . '/inc/api.php';
$rest = new API();

// Validar el api key
$headers = apache_request_headers();
if(!isset($headers['Authorization']))
{
    header("HTTP/1.1 401 Unauthorized");
    echo json_encode(['message' => 'unauthorized']);
    exit;
}

if(!$rest->validate_credentials($headers['Authorization']))
{
    header("HTTP/1.1 401 Unauthorized");
    echo json_encode(['message' => 'unauthorized']);
    exit;
}

//Obtener parametros
$params = file_get_contents('php://input');

if($method == 'obtener-saldo')
    $rest->obtener_saldo($params);
else
    $rest->{$method}($params);
