<?php
class API {


    public function venta($params)
    {
        $response = $this->apiCurl('venta', $params);
        $this->output($response);
    }

    public function ajuste($params)
    {
        $response = $this->apiCurl('ajuste', $params);
        $this->output($response);
    }

    public function consultar($params)
    {
        $response = $this->apiCurl('consultar', $params);
        $this->output($response);
    }

    public function eliminar($params)
    {
        $response = $this->apiCurl('eliminar', $params);
        $this->output($response);
    }

    public function obtener_saldo($params)
    {
        $response = $this->apiCurl('obtener_saldo', $params);
        $this->output($response);
    }

    public function validate_credentials($auth)
    {
        $auth = explode(' ', $auth);
        list($username, $apikey) = explode(':', base64_decode($auth[1]));
        $response = $this->apiCurl('validar_usuario', json_encode(['username' => $username, 'apikey' => $apikey]));
        $m_response = json_decode($response,true);
        if(!isset($m_response['success']))
            return false;
            
        return true;
    }

    /**
     * Funcion que hace la conexion con el api del enlace
     */
    private function apiCurl($method, $params)
    {
        $curl = curl_init();
        curl_setopt_array($curl, [
            CURLOPT_URL => 'http://telcel-enlace.test/api/v1/' . $method,
            CURLOPT_POST => 1,
            CURLOPT_POSTFIELDS => http_build_query(json_decode($params, true)),
            CURLOPT_RETURNTRANSFER => 1
        ]);

        $response = curl_exec($curl);

        if (curl_errno($curl)) 
        {
            $error_msg = curl_error($curl);
        }
        curl_close($curl);
        if(isset($error_msg))
            return ["error" => ["code" => 500, "message" => $error_msg]];
            
        return $response;
    }

    /** Rerotnar la respuesta del enlace */
    private function output($params)
    {
        header("Content-Type: application/json");
        echo $params;
    }
}